import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css'
import VueSweetalert2 from 'vue-sweetalert2';

Vue.config.productionTip = false
Vue.prototype.$http = Axios
Vue.use(VueSweetalert2);
Vue.use(VueSweetalert2, options);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674',
};